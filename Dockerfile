FROM python
COPY . /app
WORKDIR /app
ENTRYPOINT [ "python" ]
CMD [ "hello-world.py" ]